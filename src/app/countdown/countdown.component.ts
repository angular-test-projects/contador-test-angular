import { Component, Input, Output, OnInit, EventEmitter, OnDestroy, OnChanges } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy, OnChanges {

  @Input() init: number = null;
  @Output() onDecrease = new EventEmitter<number>();
  @Output() onComplete = new EventEmitter<void>();
  private countDownTimerRef: any = null;
  public counter: number = 0;
  constructor() { }

  ngOnChanges(changes) {
    this.startCountDown();
  }

  ngOnInit() {
    this.startCountDown();
  }

  ngOnDestroy() {
    this.clearTimeout();
  }

  private clearTimeout() {
    if (this.countDownTimerRef) {
      clearTimeout(this.countDownTimerRef);
      this.countDownTimerRef = null;
    }
  }
  startCountDown() {

    if (this.init && this.init > 0) {
      this.clearTimeout();
      this.counter = this.init;
      this.doCountDown();
    }
  }

  doCountDown() {
    this.countDownTimerRef = setTimeout(() => {
      this.counter = this.counter - 1;
      this.processCountDown();
    }, 1000)
  }

  processCountDown() {
    this.onDecrease.emit(this.counter);
    if (this.counter == 0) {
      this.onComplete.emit();
    } else {
      this.doCountDown();
    }
  }
}
